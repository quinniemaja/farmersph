import '../styles/Comment.css';
import { Fragment } from 'react';

export default function Comment(comment){
	let {commentName, commentDate, commentDescription} = comment

	const monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"
	];

	let month = monthNames[commentDate.getMonth()];
	let year = commentDate.getFullYear();
	let date = commentDate.getDate(); 

	let comment_date = `${month} ${date}, ${year}`;

	console.log(comment_date);
	return <Fragment>
		<div className="comment-container">
			<div className="comment-header">
				<p className="comment-name">{commentName}</p>
				<p className="comment-date">{comment_date}</p>
			</div>
			<p>Stars</p>
			<p className="comment-description">{commentDescription}</p>
		</div>
	</Fragment>	
}